from django.shortcuts import render
from .forms import TheForm
from .models import AddStatus
# Create your views here.

#hi
def home(request):
    if request.method == 'POST':
        form = TheForm(request.POST)
        if form.is_valid():
            data_item = form.save(commit=False)
            data_item.save()
            return render(request, 'MyApp/HelloWorld.html')
    allStatus = AddStatus.objects.all()
    form = TheForm()
    return render(request, 'MyApp/HelloWorld.html', {'form' : form, 'allStatus':allStatus})
    
