from django.forms import ModelForm
from . import models

class TheForm(ModelForm):
    class Meta:
        model = models.AddStatus
        fields = '__all__'